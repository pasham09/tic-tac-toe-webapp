package com.example.tictactoe.models.game;

import org.junit.jupiter.api.Test;
import org.springframework.util.Assert;

public class BoardTests {
  int[] boardArray = {1, 0, -1, 0, 0, 0, 0, 0, 0};

  @Test
  void testAvailablePositions() {
    Board board = new Board(Game.BOARD_ROWS, Game.BOARD_COLUMNS);
    Assert.isTrue(board.getAvailablePositions().size() == 9, "Method returns 9 positions for new board.");

    board.setCell(0, 1);
    Assert.isTrue(board.getAvailablePositions().size() == 8, "Method returns 8 positions for board with x in first cell.");
  }

}
