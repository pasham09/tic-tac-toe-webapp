package com.example.tictactoe.models.game.ai;

import static org.junit.jupiter.api.Assertions.assertTrue;

import com.example.tictactoe.models.game.Board;
import com.example.tictactoe.models.game.Game;

import org.junit.jupiter.api.Test;

public class QBotTests {

  @Test
  void testChooseAction() {
    QBot bot = new QBot(1); 
    Board board = new Board(Game.BOARD_ROWS, Game.BOARD_COLUMNS);
    int action = bot.chooseAction(board, true);
    assertTrue(action >= 0 && action < 9, "Action is in board indexes range.");
  } 
}
