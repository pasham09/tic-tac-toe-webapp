package com.example.tictactoe.models.game;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import com.example.tictactoe.models.game.ai.QBot;

import org.junit.jupiter.api.Test;

public class GameTests {

    Game game;

    @Test
    void testGameResult() {
      QBot firstBot = new QBot(1);
      QBot secondBot = new QBot(-1);
      game = new Game(firstBot, secondBot);

      assertNull(game.getResult());

      for (int i = 0; i < 9; i++) {
        game.makeMove();
      }

      assertNotNull(game.getResult());
    }
}
