package com.example.tictactoe;

import static org.assertj.core.api.Assertions.assertThat;

import com.example.tictactoe.controllers.AuthController;
import com.example.tictactoe.controllers.HomeController;
import com.example.tictactoe.controllers.QBotController;
import com.example.tictactoe.controllers.UserController;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class AiApplicationTests {

  @Autowired
  private HomeController homeController;
  @Autowired
  private AuthController authController;
  @Autowired
  private QBotController qBotController;
  @Autowired
  private UserController userController;

	@Test
	void contextLoads() {
    assertThat(homeController).isNotNull();
    assertThat(authController).isNotNull();
    assertThat(qBotController).isNotNull();
    assertThat(userController).isNotNull();
	}
}
