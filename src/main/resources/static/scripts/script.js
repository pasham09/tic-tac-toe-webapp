let playerSymbol = 1;
let currentPlayer = 1;
let board = [
  0, 0, 0,
  0, 0, 0,
  0, 0, 0
];

const cells = document.querySelectorAll('.cell');
cells.forEach((cell, index) => {
  cell.addEventListener('click', () => handleCellClick(cell, index));
});

const playerSymbolEl = document.getElementById('player-symbol');
playerSymbolEl.addEventListener('click', handlePlayerSymbolClick);

const buttonReset = document.getElementById('reset');
buttonReset.addEventListener('click', () => {
  resetGame();
  document.querySelector('.overlay').style.display = "none";
});

/**
 * Resets the game, so player could start new one.
 */
const resetGame = () => {
  board = [
    0, 0, 0,
    0, 0, 0,
    0, 0, 0
  ];

  currentPlayer = 1;
  cells.forEach(cell => {
    cell.innerHTML = '' ;
  });

  if (playerSymbol === -1) {
    findNextCell().then(nextCell => {
      updateCellWithX(nextCell)
      currentPlayer = -currentPlayer;
    });
  }
};


/**
 * Handles players action to change symbol to play.
 * Resets the game and updates UI with new symbol.
 */
function handlePlayerSymbolClick() {
  playerSymbol = -playerSymbol;
  resetGame();

  if (playerSymbol === 1) {
    playerSymbolEl.classList.remove('o-symbol');
    playerSymbolEl.classList.add('x-symbol');
  } else if (playerSymbol === -1) {
    playerSymbolEl.classList.remove('x-symbol');
    playerSymbolEl.classList.add('o-symbol');
  }
};

/**
 * Handles players action to make move.
 * Updates UI with players symbol and finds next move.
 * @param cell 
 * @param index Position of the cell in board array.
 */
async function handleCellClick(cell, index) {
  if (cell.hasChildNodes())
    return;
  
  board[index] = currentPlayer;

  if (currentPlayer === 1) {
    updateCellWithX(cell); 
  } else if (currentPlayer === -1) {
    updateCellWithO(cell);
  }

  if (isWin()) {
    document.querySelector('.overlay-message').textContent = `${currentPlayer === 1 ? 'X' : 'O'} has won!`;
    document.querySelector('.overlay').style.display = "block";

    if (currentPlayer === playerSymbol) {
      incrementUserWins();
    } else {
      incrementUserLoses();
    }

    return;
  } else if (!hasAvailablePositions()) {
    document.querySelector('.overlay-message').textContent = `Draw!`;
    document.querySelector('.overlay').style.display = "block";
    return;
  }

  currentPlayer = -currentPlayer;
  const nextCell = await findNextCell();

  if (currentPlayer === 1) {
    updateCellWithX(nextCell); 
  } else if (currentPlayer === -1) {
    updateCellWithO(nextCell);
  }

  if (isWin()) {
    document.querySelector('.overlay-message').textContent = `${currentPlayer === 1 ? 'X' : 'O'} has won!`;
    document.querySelector('.overlay').style.display = "block";

    if (currentPlayer === playerSymbol) {
      incrementUserWins();
    } else {
      incrementUserLoses();
    }

    return;
  } else if (!hasAvailablePositions()) {
    document.querySelector('.overlay-message').textContent = `Draw!`;
    document.querySelector('.overlay').style.display = "block";
    return;
  }

  currentPlayer = -currentPlayer;
};

async function findNextCell() {
  const nextMove = await fetchNextMove();
  board[nextMove] = currentPlayer;
  const nextCell = document.querySelectorAll('.cell')[nextMove];

  return nextCell;
}

/**
 * Fetches next move from back-end.
 * @returns {Integer} Position on the board to move to.
 */
async function fetchNextMove() {
  const response = await fetch(`/bot/play/${-playerSymbol}`, {
    method: 'POST',
    headers: {
     'Content-type': 'application/json',
    },
    body: JSON.stringify(board)
  });

  const nextMove = await response.json();
  return nextMove;
};

async function incrementUserWins() {
  await fetch('/user/updatewins', { method: 'POST' });
}

async function incrementUserLoses() {
  await fetch('/user/updateloses', { method: 'POST' });
}

/**
 * Updates specified cell with X symbol.
 * @param cell 
 */
function updateCellWithX(cell) {
  const divX = document.createElement('div');
  divX.classList.add("x-symbol");
  cell.appendChild(divX);
};


/**
 * Updates specified cell with O symbol.
 * @param cell 
 */
function updateCellWithO(cell) {
  const divO = document.createElement('div');
  divO.classList.add("o-symbol");
  cell.appendChild(divO);
}

const winningConditions = [
  [0, 1, 2],
  [3, 4, 5],
  [6, 7, 8],
  [0, 3, 6],
  [1, 4, 7],
  [2, 5, 8],
  [0, 4, 8],
  [2, 4, 6]
];

/**
 * Checks if there is win condition on the board.
 * @returns true if there is a win on the board or false.
 */
function isWin() {
  for (let i = 0; i <= 7; i++) {
    const winCondition = winningConditions[i];

    let a = board[winCondition[0]];
    let b = board[winCondition[1]];
    let c = board[winCondition[2]];

    if (a === 0 || b === 0 || c === 0) {
      continue;
    }

    if (a === b && b === c) {
      return true;
    }
  }
  return false;
}

function hasAvailablePositions() {
  for (let i = 0; i < board.length; i++) {
    if (board[i] === 0)
      return true;
  }

  return false;
}