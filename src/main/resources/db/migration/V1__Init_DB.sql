create table bots (
  id integer not null,
  symbol integer not null,
  primary key (id)
);

create table hibernate_sequence (
  next_val bigint
);

insert into hibernate_sequence values (1);

create table states_value (
  bot_id integer not null,
  state_value double precision,
  state_hash integer not null,
  primary key (bot_id, state_hash)
);

create table user (
  id integer not null,
  active bit not null,
  loses_count integer,
  password varchar(255) not null,
  roles varchar(255) not null, 
  user_name varchar(255) not null,
  wins_count integer,
  primary key (id)
); 

alter table states_value 
  add constraint bot_fk
  foreign key (bot_id) references bots (id);