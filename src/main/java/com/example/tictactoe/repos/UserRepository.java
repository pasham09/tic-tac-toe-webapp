package com.example.tictactoe.repos;

import java.util.Optional;

import com.example.tictactoe.models.User;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface UserRepository extends CrudRepository<User, Integer> {
  Optional<User> findByUserName(String userName);

  @Modifying
  @Transactional
  @Query("update User u set u.winsCount = :winsCount where u.userName = :userName")
  void updateWins(@Param(value = "userName") String userName, @Param(value = "winsCount") int winsCount);

  @Modifying
  @Transactional
  @Query("update User u set u.losesCount = :losesCount where u.userName = :userName")
  void updateLoses(@Param(value = "userName") String userName, @Param(value = "losesCount") int losesCount);

}
