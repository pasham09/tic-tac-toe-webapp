package com.example.tictactoe.repos;

import java.util.Optional;

import com.example.tictactoe.models.game.ai.QBot;

import org.springframework.data.repository.CrudRepository;

public interface QBotRepository extends CrudRepository<QBot, Integer> {
  Optional<QBot> findFirstBySymbol(int symbol);
}
