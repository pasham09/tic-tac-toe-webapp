package com.example.tictactoe.configurations;

import java.util.Collections;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
public class SpringFoxConfig {
  @Bean
  public Docket api() {
    return new Docket(DocumentationType.SWAGGER_2)
      .select()
      .apis(RequestHandlerSelectors.basePackage("com.example.tictactoe"))
      .build()
      .apiInfo(getApiInfo());
  } 

  private ApiInfo getApiInfo() {
    return new ApiInfo(
      "Tictactoe AI API.",
      "API for managing AI bots.",
      "1.0",
      null,
      new Contact("Pavel Moskalenko", "https://github.com/sydneyidler", "pasha.moskalenko.99@gmail.com"),
      null,
      null,
      Collections.emptyList());
  }
}
