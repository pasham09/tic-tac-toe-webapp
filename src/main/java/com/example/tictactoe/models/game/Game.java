package com.example.tictactoe.models.game;

import com.example.tictactoe.models.game.ai.QBot;

public class Game {

  public static final int BOARD_ROWS = 3;
  public static final int BOARD_COLUMNS = 3;

  private int playerToMoveSymbol;
  private Board board;
  private QBot firstBot;
  private QBot secondBot;

  public Game(QBot firstBot, QBot secondBot) {
    playerToMoveSymbol = 1;
    board = new Board(Game.BOARD_ROWS, Game.BOARD_COLUMNS);

    if (firstBot.getSymbol() == 1 && secondBot.getSymbol() == -1) {
      this.firstBot = firstBot;
      this.secondBot = secondBot;
    } else {
      this.firstBot = secondBot;
      this.secondBot = firstBot;
    }
  }

  /**
   * Chooses bot to move next and makes next move for it.
   */
  public void makeMove() {
    QBot botToMove = playerToMoveSymbol == 1 ? firstBot : secondBot;

    int botAction = botToMove.chooseAction(board, true);

    board.setCell(botAction, playerToMoveSymbol);
    playerToMoveSymbol = -playerToMoveSymbol; 

    botToMove.addState(board.hashCode());
  }

  /**
   * Returns the game result.
   * @return Result of the game.
   */
  public GameResult getResult() {
    GameResult gameResult = new GameResult();

    for (int i = 0; i < 8; i++) {
      int sum = 0;
      switch (i) {
        case 0:
          sum = board.getCell(0) + board.getCell(1) + board.getCell(2);
          break;
        case 1:
          sum = board.getCell(3) + board.getCell(4) + board.getCell(5);
          break;
        case 2:
          sum = board.getCell(6) + board.getCell(7) + board.getCell(8);
          break;
        case 3:
          sum = board.getCell(0) + board.getCell(3) + board.getCell(6);
          break;
        case 4:
          sum = board.getCell(1) + board.getCell(4) + board.getCell(7);
          break;
        case 5:
          sum = board.getCell(2) + board.getCell(5) + board.getCell(8);
          break;
        case 6:
          sum = board.getCell(0) + board.getCell(4) + board.getCell(8);
          break;
        case 7:
          sum = board.getCell(2) + board.getCell(4) + board.getCell(6);
          break;
      }

      if (sum == 3) {
        gameResult.setWinner(firstBot);
        gameResult.setLoser(secondBot);

        return gameResult;
      } else if (sum == -3) {
        gameResult.setWinner(secondBot);
        gameResult.setLoser(firstBot);

        return gameResult;
      } 
    } 

    if (board.getAvailablePositions().size() == 0) {
      gameResult.setIsDraw(true);

      return gameResult;
    }

    return null;
  } 
}
