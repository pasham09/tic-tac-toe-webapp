package com.example.tictactoe.models.game;

import com.example.tictactoe.models.game.ai.QBot;

public class GameResult {
  private QBot winner;
  private QBot loser;
  private boolean isDraw;

  public void setWinner(QBot winner) {
    this.winner = winner;
  }

  public QBot getWinner() {
    return winner;
  }

  public void setLoser(QBot loser) {
    this.loser = loser;
  }

  public QBot getLoser() {
    return loser;
  }

  public void setIsDraw(boolean isDraw) {
    this.isDraw = isDraw;
  }

  public boolean getIsDraw() {
    return isDraw;
  }
}
