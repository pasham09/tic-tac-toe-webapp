package com.example.tictactoe.models.game.ai;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapKeyColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.example.tictactoe.models.game.Board;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "bots")
@ApiModel(description = "Details about QBot.")
public class QBot {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @ApiModelProperty(notes = "The unique id of the bot.")
  private Integer id;

  @ElementCollection
  @CollectionTable(name = "states_value", joinColumns = {@JoinColumn(name = "bot_id", referencedColumnName = "id")})
  @MapKeyColumn(name = "state_hash")
  @Column(name = "state_value")
  @ApiModelProperty(notes = "Values for each board position.")
  private Map<Integer, Double> statesValue;

  @ApiModelProperty(notes = "The symbol that the bot learned to play.")
  private int symbol;

  @Transient
  private List<Integer> states;
  @Transient
  private double lr = 0.2;
  @Transient
  private double decayGamma = 0.9;
  @Transient
  private double expRate = 0.3;

  public QBot() {
    this.symbol = 1;
  }

  public QBot(int symbol) {
    this.symbol = symbol;
    states = new ArrayList<Integer>();
    statesValue = new HashMap<Integer, Double>();
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public void setStatesValue(Map<Integer, Double> statesValue) {
    this.statesValue = statesValue;
  }

  public void setSymbol(int symbol) {
    this.symbol = symbol;
  }

  public Integer getId() {
    return id;
  }

  public Map<Integer, Double> getStatesValue() {
    return statesValue;
  }

  public int getSymbol() {
    return symbol;
  }

  /**
   * Choose the best action for the given position, or with the chance of expRate choose a random position.
   * @param curBoard Position on the board.
   * @param isTraining True if bot is training.
   * @return Integer that represents cell on the board.
   */
  public int chooseAction(Board curBoard, boolean isTraining) {
    int action = 0;
    List<Integer> availablePositions = curBoard.getAvailablePositions();

    if (isTraining && Math.random() <= expRate) {
      int idx = (int) (Math.random() * availablePositions.size());
      action = availablePositions.get(idx);
    } else {
      double valueMax = -999;

      for (Integer p : availablePositions) {
        Board nextBoard = new Board(curBoard.getBoardArray());
        nextBoard.setCell(p, symbol);
        int nextBoardHash = nextBoard.hashCode(); 

        double value = statesValue.get(nextBoardHash) == null ? 0 : statesValue.get(nextBoardHash);
        if (value > valueMax) {
          valueMax = value;
          action = p;
        }
      }
    }

    return action;
  }

  /**
   * Updates values of statesValue map based on the reward.
   * @param reward Number that represent award for the result of the game.
   */
  public void feedReward(double reward) {
    for (int i = states.size() - 1; i > -1; i--) {
      int st = states.get(i);
      if (statesValue.get(st) == null)
        statesValue.put(st, 0.0);

      statesValue.put(st, lr * (decayGamma * reward - statesValue.get(st)));
      reward = statesValue.get(st);
    }
  }

  public void reset() {
    states = new ArrayList<Integer>();
  }

  public void addState(int state) {
    states.add(state);
  }
}
