package com.example.tictactoe.models.game;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Board {
  private int[] board; 

  public Board(int rows, int columns) {
    board = new int[rows * columns];
  }

  public Board(int[] board) {
    this(Game.BOARD_ROWS, Game.BOARD_COLUMNS);
    System.arraycopy(board, 0, this.board, 0, board.length);
  }

  public int[] getBoardArray() {
    return board;
  }

  public int hashCode() {
    return Arrays.hashCode(board);
  }

  public int getCell(int index) {
    return board[index]; 
  }

  public void setCell(int position, int symbol) {
    board[position] = symbol;
  }

  public List<Integer> getAvailablePositions() {
    List<Integer> list = new ArrayList<>(); 

    for (int i = 0; i < board.length; i++) {
      if (board[i] == 0)
        list.add(i);
    }

    return list;
  } 
}
