package com.example.tictactoe.models.game.ai;

import com.example.tictactoe.models.game.Game;
import com.example.tictactoe.models.game.GameResult;

public class QLearner {
  
  /**
   * Trains QBot with another QBot for specified number of games.
   * @param bot Bot to train.
   * @param rounds Amount of games bot will play.
   */
  public static void trainQBot(QBot bot, int rounds) {
    QBot botToTrainWith = new QBot(-bot.getSymbol()); 

    for (int i = 0; i < rounds; i++) {
      Game game = new Game(bot, botToTrainWith);
      bot.reset();
      botToTrainWith.reset();

      while (true) {
        game.makeMove();
        GameResult gameResult = game.getResult();

        if (gameResult == null)
          continue;

        if (gameResult.getIsDraw()) {
          bot.feedReward(0.3);
          botToTrainWith.feedReward(0.3);
          break;
        } else {
          gameResult.getWinner().feedReward(1);
          gameResult.getLoser().feedReward(0);
          break;
        }
      }
    }
  }
  
}