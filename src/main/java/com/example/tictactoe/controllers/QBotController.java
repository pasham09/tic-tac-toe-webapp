package com.example.tictactoe.controllers;

import java.util.Optional;

import com.example.tictactoe.models.game.ai.QBot;
import com.example.tictactoe.models.game.ai.QLearner;
import com.example.tictactoe.models.game.Board;
import com.example.tictactoe.repos.QBotRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/bot")
public class QBotController {

  @Autowired
  private QBotRepository qBotRepository;

  @PostMapping("/add")
  @ApiOperation(value = "Add new bot to the database.",
    notes = "Specify which symbol bot will learn to play.(1 = x or -1 = o)")
  public String addNewBot(
    @ApiParam(value = "Symbol that bot will learn to play.", required = true)
    @RequestBody int botSymbol) {

    QBot qBot = new QBot(botSymbol);
    QLearner.trainQBot(qBot, 5000000);
    qBotRepository.save(qBot);

    return "Success";
  }

  @PostMapping("/play/{symbol}")
  @ApiOperation(value = "Finds next move for provided position based on the bot knowledge.",
    notes = "Provide an array of integers that represents board.")
  public Integer play(
      @PathVariable Integer symbol,
      @ApiParam(value = "An array of integers (1, 0, -1) that represents board.", required = true)
      @RequestBody int[] board) {

    Optional<QBot> queryResult = qBotRepository.findFirstBySymbol(symbol);

    if (!queryResult.isPresent())
      return -1;

    Board newBoard = new Board(board);
    QBot qBot = queryResult.get();
    int action = qBot.chooseAction(newBoard, false);

    return action;
  }

  @GetMapping("/all")
  public Iterable<QBot> getAll() {
    return qBotRepository.findAll();
  }
}

