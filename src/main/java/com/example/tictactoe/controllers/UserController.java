package com.example.tictactoe.controllers;

import com.example.tictactoe.models.User;
import com.example.tictactoe.repos.UserRepository;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
@RequestMapping("/user")
public class UserController {
  @Autowired
  private UserRepository userRepository;

  @GetMapping("/profile")
  public String profile(Principal principal, Model model) {
    User user = userRepository.findByUserName(principal.getName()).get();
    model.addAttribute("wins", user.getWinsCount());
    model.addAttribute("loses", user.getLosesCount());
    return "profile";
  }

  @PostMapping("/updatewins")
  @ResponseStatus(value = HttpStatus.OK)
  public void updateWins(Principal principal) {
    String userName = principal.getName();
    User user = userRepository.findByUserName(userName).get();
    userRepository.updateWins(userName, user.getWinsCount() + 1);
  }

  @PostMapping("/updateloses")
  @ResponseStatus(value = HttpStatus.OK)
  public void updateLoses(Principal principal) {
    String userName = principal.getName();
    User user = userRepository.findByUserName(userName).get();
    userRepository.updateLoses(userName, user.getLosesCount() + 1);
  }
}
