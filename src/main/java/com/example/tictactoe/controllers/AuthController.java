package com.example.tictactoe.controllers;

import java.util.Optional;

import com.example.tictactoe.models.User;
import com.example.tictactoe.repos.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class AuthController {
  @Autowired
  private UserRepository userRepository;

  @Autowired
  private PasswordEncoder passwordEncoder;

  @GetMapping("login")
  public String login() {
    return "login";
  }

  @GetMapping("register")
  public String registerForm() {
    return "register";
  }

  @PostMapping("register")
  public String registerSubmit(@ModelAttribute User user) {
    Optional<User> userFromDb = userRepository.findByUserName(user.getUserName());
    if (userFromDb.isPresent())
      return "redirect:/register?error";

    user.setActive(true);
    user.setRoles("ROLE_USER");
    user.setPassword(passwordEncoder.encode(user.getPassword()));
    userRepository.save(user); 
    return "redirect:/login";
  }
}
